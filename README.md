# Prérequis

Ce projet a pour seul dépendance

```Docker Engine 17.06.0+```

Votre machine doit également avoir le port 80 disponible (services ```Apache2``` ou ```Nginx``` arrêtés).

# Installation

Une fois le projet cloné sur une machine, se placer à la racine du projet et créer un fichier ```.env``` sur le modèle de ```.env.example``` fourni.  
Puis, lancer la commande :

```docker-compose up -d```

et.. c'est tout !

# Utilisation

Accéder à la machine via son IP ou son hostname depuis n'importe quel navigateur.

Une fois sur l'interface du jeu, la prise en main est simple, le score/vies restantes est en haut de la page, le nom à deviner au milieu et les contrôles sont sur les côtés.  
Le jeu se joue au clavier, à l'aide des flêches de direction gauche et droite.

# Fonctionnement

Le projet est composé de 3 services ```Database```, ```API``` et ```Webapp```.  
Ces services sont chacun encapsulés dans un conteneur Docker, et sont mis en relations via un ```docker-compose.yml``` à la racine du projet. De ce fait, il est déployable sur n'importe quelle machine supportant Docker Engine 17.06.0+.

## Database

Ce service exécute ```MySQL 8.0.26```, qui est initialisé via les variables d'environnement placées dans le fichier ```.env```.  
Au lancement, le service execute le script ```database/init.sql```, qui va crée une table Firstnames comportant des champs ```id``` et ```firstname```.  
Puis, cette table est populée via une query ```LOAD DATA INFILE``` prenant comme fichier source ```database/names.txt``` en ignorant les prénoms dupliqués.

## API

Ce service exécute ```NodeJS 16/ Alpine 3.11```, et sert le projet NodeJS contenu dans le dossier ```api/app/``` sur le port 3000.
Ce projet NodeJS est un serveur ```express``` servant une seule route ```/firstname/random```.  
Cette route appelle une fonction ```random``` du controller ```api/app/controllers/firstnameController.js```.  
Cette fonction appelle 2 services codés dans ```api/app/services/databaseService.js``` et ```api/app/services/genderService.js```, servants respectivement à obtenir un prénom aléatoire depuis le conteneur ```Database``` via les identifiants de connexion partagés dans le ```.env```, et à obtenir le genre de ce prénom via une requête vers le service Gender API et la clé d'API contenue dans le fichier ```.env``` également. La fonction retourne un json { name, gender } qui est la réponse de la route ```/firstname/random```.

## Webapp

Ce service est le même que le service ```API```, à la différence qu'il héberge lui le front fait en VueJS, sur le port 80.

L'application VueJS ne comporte qu'une seule vue dans ```webapp/app/src/App.vue```et 5 composants contenus dans ```webapp/app/src/components/```.  
La vue ```App``` contient en données le score, la question et le nombre de questions depuis le début de la partie.
Le component ```EndScreen``` est affiché à la fin de la partie lorsque le score est égal à 0 ou 20, et affiche un message de félicitations ou de game over en fonction de ce score.
Les autres components sont utilisés au cours de la partie :  
```Header``` affiche le score/les vies en haut de la page.  
```Controls``` affiche les contrôles à gauche et à droite de l'écran avec les images de masculin et féminin.  
```Question``` affiche la question et le prénom demandé.  
L'App est initialisée en requêtant l'API pour obtenir un premier prénom, et en fixant le score à 10. Puis, l'App écoute les événements de clavier pour détecter les touches left et right et déclenche lors de leur appui la fonction qui déclenche une réponse. En cas de bonne réponse on ajoute un point, sinon on en enlève. Enfin, on change de prénom en requêtant une nouvelle fois l'API.
Lors de la requête, une variable ```fetching``` est manipulée, afin d'empêcher le spam de réponses pendant que la requête se fait et que les données n'ont pas été rafraichies.
