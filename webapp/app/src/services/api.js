const axios = require('axios');
var os = require("os");

const port = process.env.API_PORT || 3000;
console.log(`http://${os.hostname()}:${port}/firstname/random`);

module.exports = {
  getRandomQuestion: function() {
    return axios.get(`http://${os.hostname()}:${port}/firstname/random`)
    .then(res => { return { name: res.data.name, answer: res.data.gender }})
    .catch(err => console.log(err))
  }
}