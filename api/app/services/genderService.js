const axios = require('axios');

const key = process.env.GENDER_API_KEY;

module.exports = {
  getGender: function(name) {
    return axios.get('https://gender-api.com/get', { params: { name, key } })
    .then(res => res.data.gender)
    .catch(err => console.log(err))
  }
}