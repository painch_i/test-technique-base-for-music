var mysql = require('mysql8.0');

var con = mysql.createConnection({
  host: "database",
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME
});
con.connect(function(err) {
  if (err) throw err;
});


module.exports = {
  getRandomName: function() {
    const randomQuery = "SELECT firstname FROM Firstnames ORDER BY RAND() LIMIT 1"
    return new Promise(function(resolve, reject) {
      con.query(randomQuery, function (err, result) {
        if (err) return reject(err);
        resolve(result[0].firstname);
      });
    });

  }
}