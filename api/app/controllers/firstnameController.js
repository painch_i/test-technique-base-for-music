const GenderAPI = require('../services/genderService');
const Database = require('../services/databaseService');

module.exports = {
  random: async function(req, res) {
    const name = await Database.getRandomName();
    const gender = await GenderAPI.getGender(name);
    return res.status(200).json({ name, gender });
  }
}