const express = require('express');
var cors = require('cors');
const app = express();
app.use(cors())
const port = 3000;

const firstnameController = require('./controllers/firstnameController');

app.get('/firstname/random',firstnameController.random);

app.listen(port, () => {
  console.log(`Serveur en écoute sur le port ${port}`)
})
